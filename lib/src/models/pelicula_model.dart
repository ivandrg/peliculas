class Peliculas {
  // Lista del tipo pelicula, mapa con las propiedades de pelicula va a contener todas las peliculas
  List<Pelicula> items = [];
  // Consturctor de la clase
  Peliculas();
  // Constructor que recibe la lista de todas la peliculas y convertirla en items
  Peliculas.fromJsonList(List<dynamic> jsonList) {
    // Si el JSON esta vacio no hace nada
    if (jsonList == null) return;
    for (var item in jsonList) {
      // Mando los datos del JSON a la estructura de datos en pelicula para asignar cada una de sus propiedades de la clase
      final pelicula = new Pelicula.fromJsonMap(item);
      // Agrego cada item de cada pelicula a la lista items donde estan todas la peliculas
      items.add(pelicula);
    }
  }
}

// Estructura de los datos
class Pelicula {
  double popularity;
  int id;
  bool video;
  int voteCount;
  double voteAverage;
  String title;
  String releaseDate;
  String originalLanguage;
  String originalTitle;
  List<int> genreIds;
  String backdropPath;
  bool adult;
  String overview;
  String posterPath;

  Pelicula({
    this.popularity,
    this.id,
    this.video,
    this.voteCount,
    this.voteAverage,
    this.title,
    this.releaseDate,
    this.originalLanguage,
    this.originalTitle,
    this.genreIds,
    this.backdropPath,
    this.adult,
    this.overview,
    this.posterPath,
  });

  Pelicula.fromJsonMap(Map<String, dynamic> json) {
    popularity = json['popularity'] /
        1; // Se divide entre uno para que me de resultado con decimal si es entero 5->5.0
    id = json['id'];
    video = json['video'];
    voteCount = json['vote_count'];
    voteAverage = json['vote_average'] / 1;
    title = json['title'];
    releaseDate = json['release_date'];
    originalLanguage = json['original_language'];
    originalTitle = json['original_title'];
    genreIds = json['genre_ids'].cast<int>(); // Conversion a entero
    backdropPath = json['backdrop_path'];
    adult = json['adult'];
    overview = json['overview'];
    posterPath = json['poster_path'];
  }

// Metodo que devuelve la imagen o poster de la pelicula
  getPosterImg() {
    if (posterPath == null) {
      return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARAAAAC6CAMAAABRJOIbAAAAMFBMVEXm6Of////4+fnw8fHo6unt7u38/Pzk5uXz9PPw8vHx8/L3+Pfn6Oj7+vvt7+39/v2JQ8GPAAACH0lEQVR4nO3a0VLDIBCFYaALJKW27/+2AkkaQtR6VRz3/+6Io4NnFggQYwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN5CfNa3KxnWp6Emm92fTe/sJswDuzXOtf7zl7XlbWsa2rNBxDWJ3MIhEOtf/PK/dPvYE5mPeVg3unNDSFrGh4h0BVJKRBajO/lWayIuuT4PG9Jq0hOJ+C2RH4X76z/1P7g8ffwmEaukRspCO5dYXlKyCJeFJcj6grYNj+wciJIlZ14qZE8kTD6vKn4+1YySQC7LYHjWSHj+xAe1gdjp4qYtkSQ1HmP6qVZTIDWUuCbirim4+t5xc5oDsaltLJVy2NuoC6QkMnUB9G0F2qLwx2ZefOShOpAybzQb3vJ64lQHUh9c90ByxSTNgYRoJK8u+6mZb+PRGEg5P3PNI3+YVfUFUuaQZJ2Y++M5h6geMqUi8ngpiWxzSHuGpjCQ/CJSSsKZeFlLpD2FVxhIPWVPJYi47uyc8kBc2djljX+ePfzDdjQGUjcwRqRcY/a7f52BWLffTfWXEjoDsWGSmB9HMbEbNUoDKZm4lIJ9+HisESWB+HMgWy5yTCSN7up7nK8v90SOL2Zabr5Pq0mbSFMjSq5lMv/9td1eI0FLfby2fkCi83OiLx0+qYGhRr5QEwlxdDf+juVbzdG9AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIb6BG0BCzA/r/PHAAAAAElFTkSuQmCC';
    } else {
      return 'https://image.tmdb.org/t/p/w500/$posterPath';
    }
  }
}
