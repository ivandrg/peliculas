import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:peliculas/src/models/pelicula_model.dart';

class PeliculasProvider {
  String _apikey = '7cb08b3e6599903ee84bcfdaca4e12b5';
  String _url = 'api.themoviedb.org';
  String _language = 'es-ES';

  int _popularesPage = 0;
  bool _cargando = false;
  // Lista vacia de peliculas
  List<Pelicula> _populares = [];
  // Controlador de stream el broadcast hace que pueda ser escuchado desde muchos lugares
  final _popularesStreamController =
      StreamController<List<Pelicula>>.broadcast();
  // La funcion espera una lista de peliculas y agrega peliculas del stream controller
  Function(List<Pelicula>) get popularesSink =>
      _popularesStreamController.sink.add;
  // Genera el stream con la data que se agrega
  Stream<List<Pelicula>> get popularesStream =>
      _popularesStreamController.stream;

  void disposeStream() {
    // Si el stream esta activo lo cierra
    _popularesStreamController?.close();
  }

  Future<List<Pelicula>> _procesarRespuesta(Uri url) async {
    // Metodo get que devuelve la respuesta de la api
    final resp = await http.get(url);
    // Decodifica la data del archivo JSON en un MAP para ser usada
    final decodeData = json.decode(resp.body);
    // Acopla los datos del archivo decodificado en la estructura creada para peliculas
    final peliculas = new Peliculas.fromJsonList(decodeData['results']);
    return peliculas.items;
  }

// Metodo future que devuelve la respuesta de la api por get
  Future<List<Pelicula>> getEnCines() async {
    // Comando que construye una url con los datos
    final url = Uri.http(_url, '3/movie/now_playing',
        {'api_key': _apikey, 'language': _language});
    return await _procesarRespuesta(url);
  }

  Future<List<Pelicula>> getPopular() async {
    if (_cargando) return [];
    _cargando = true;
    _popularesPage++;
    print('Cargar siguientes peliculas');
    final url = Uri.http(_url, '3/movie/popular', {
      'api_key': _apikey,
      'language': _language,
      'page': _popularesPage.toString()
    });
    final resp = await _procesarRespuesta(url);
    // Agrega a populares todas las peliculas consultadas por el api
    _populares.addAll(resp);
    // Se manda la data con sink al stream
    popularesSink(_populares);
    _cargando = false;
    return resp;
  }
}
