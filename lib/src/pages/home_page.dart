import 'package:flutter/material.dart';

import 'package:peliculas/src/provider/peliculas.provider.dart';
import 'package:peliculas/src/widgets/card_swiper_widget.dart';
import 'package:peliculas/src/widgets/movies_horizontal.dart';

class HomePage extends StatelessWidget {
  // Instancia la clase/propiedad de clase
  final peliculasProvider = new PeliculasProvider();

  @override
  Widget build(BuildContext context) {
    peliculasProvider.getPopular();
    return Scaffold(
        appBar: AppBar(
          title: Text('Peliculas en cines'),
          backgroundColor: Colors.indigoAccent,
          actions: <Widget>[
            IconButton(icon: Icon(Icons.search), onPressed: () {})
          ],
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              _swiperTarjetas(),
              _footer(context),
            ],
          ),
        ));
  }

  Widget _swiperTarjetas() {
    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return CardSwiper(peliculas: snapshot.data);
        } else {
          return Container(
            height: 400.0,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
        // Llama al metodo getencines
      },
    );
  }

  Widget _footer(BuildContext context) {
    return Container(
      // Agarra todo el espacio horizontal
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 20.0),
            // texto con las propiedades del tema como subtitulos 1 se puede modificar el tema para que cambien en todas los textos
            child:
                Text('Populares', style: Theme.of(context).textTheme.subtitle1),
          ),
          SizedBox(
            height: 5.0,
          ),

          StreamBuilder(
            stream: peliculasProvider.popularesStream,
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              // snapshot.data?.forEach((element) => print(element.title));
              if (snapshot.hasData) {
                return MovieHorizontal(
                    peliculas: snapshot.data,
                    siguientePagina: peliculasProvider.getPopular);
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
          // FutureBuilder(
          //   future: peliculasProvider.getPopular(),
          //   builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          //     // snapshot.data?.forEach((element) => print(element.title));
          //     if (snapshot.hasData) {
          //       return MovieHorizontal(peliculas: snapshot.data);
          //     } else {
          //       return Center(child: CircularProgressIndicator());
          //     }
          //   },
          // ),
        ],
      ),
    );
  }
}
