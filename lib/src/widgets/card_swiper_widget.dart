import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import 'package:peliculas/src/models/pelicula_model.dart';

class CardSwiper extends StatelessWidget {
  final List<Pelicula> peliculas;

  CardSwiper({@required this.peliculas});

  @override
  Widget build(BuildContext context) {
// Entrega el tamaño de pantalla asi como otros elementos si se cambia la palabra size
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.only(top: 10.0),
      // Se deben colocar estas medidas para que se muestre el swiper
      // width: _screenSize.width * 0.7,
      // height: _screenSize.height * 0.5,
      // Widget para mostrar las tarjetas para deslizar
      child: Swiper(
        // Opcion para modificar el modelo del swiper
        layout: SwiperLayout.STACK,
        itemWidth: _screenSize.width * 0.7,
        itemHeight: _screenSize.height * 0.5,
        // itemWidth: 288,
        // itemHeight: 398,
        itemCount: peliculas.length,
        itemBuilder: (BuildContext context, int index) {
          // Redondea los border del swiper
          return ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: FadeInImage(
              placeholder: AssetImage('assets/img/no-image.jpg'),
              image: NetworkImage(peliculas[index].getPosterImg()),
              fit: BoxFit.cover,
            ),
          );
        },
        // pagination: new SwiperPagination(),
        // control: new SwiperControl(),
      ),
    );
  }
}
