import 'package:flutter/material.dart';
import 'package:peliculas/src/models/pelicula_model.dart';

class MovieHorizontal extends StatelessWidget {
  // Lista de pelculas
  final List<Pelicula> peliculas;
  // Funcion para generar las peliculas
  final Function siguientePagina;
  // Instancia peliculas y hace que sea obligatoria pasar la lista
  MovieHorizontal({@required this.peliculas, @required this.siguientePagina});
  final _pageController = new PageController(
    initialPage: 1,
    viewportFraction: 0.3,
  );
  @override
  Widget build(BuildContext context) {
    // Tamaño de la pantalla
    final _screenSize = MediaQuery.of(context).size;
    // Posicion en pixeles del page controller
    _pageController.addListener(() {
      if (_pageController.position.pixels >=
          _pageController.position.maxScrollExtent - 200) {
        siguientePagina();
      }
    });
    return Container(
      // Medida del container
      height: _screenSize.height * 0.3,
      // Widget para ver las peliculas de manera horizontal
      child: PageView.builder(
        // Hace que corra mas rapido la barra de peliculas
        pageSnapping: false,
        // Configura cuantas peluculas se pueen ver en la pantalla y con cual empieza
        controller: _pageController,
        itemCount: peliculas.length,
        itemBuilder: (context, i) {
          return _tarjeta(context, peliculas[i]);
        },
        // // Se pasa la lista con las peliculas
        // children: _tarjetas(context),
      ),
    );
  }

  Widget _tarjeta(BuildContext context, Pelicula pelicula) {
    return Container(
      margin: EdgeInsets.only(right: 15.0),
      child: Column(
        children: <Widget>[
          // Para hacer los bordes redondeados
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: FadeInImage(
              placeholder: AssetImage('assets/img/no-image.jpg'),
              image: NetworkImage(pelicula.getPosterImg()),
              fit: BoxFit.cover,
              height: 160.0,
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Text(
            pelicula.title,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.caption,
          )
        ],
      ),
    );
  }

  List<Widget> _tarjetas(BuildContext context) {
    // Manera para recorrer la lista de peliculas
    return peliculas.map((pelicula) {
      // Convierte el MAP en un lista para ser devuelta
    }).toList();
  }
}
